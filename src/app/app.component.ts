import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hangedman-game';

  @ViewChild('child') input!: ElementRef<any>;

  erros = 0
  totalTries = 5
  words = [
    ['p', 'a', 'l', 'a', 'v', 'r', 'a'],
    ['v', 'a', 's', 's', 'o', 'u', 'r', 'a'],
    ['o', 'v', 'o'],
  ]
  palavra: string[] = []
  found: string[] = []
  over = false;
  win = false;

  constructor() {

    
    this.palavra = this.words[Math.trunc(Math.random()*this.words.length)]

    this.found = this.palavra.map((l: string)=>{
      return '_'
    })
  }

  manda() {
    if(!this.win && !this.over){

      var letter = this.input.nativeElement.value[0];
      var foundLetter = false;
  
      for (let i = 0; i < this.palavra.length; i++) {
        if(letter == this.palavra[i]){
          this.found[i] = letter;
          foundLetter = true;
        }
      }
  
      if(!foundLetter) this.erros++
  
      if(this.erros >= this.totalTries) this.over = true
      
      var empty = 0;
      for (let i = 0; i < this.found.length; i++) {
        if(this.found[i] == '_') empty++;
      }
  
      if(empty == 0) this.win = true;
    }
    
  }
}
